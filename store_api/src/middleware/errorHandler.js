// imports 
const { CustomAPIError } = require("../helpers/handleError");


// start
const errorHandlerMiddleware = (err, req, res, next) => {
    // console.log(err);
    if (err instanceof CustomAPIError) return res.status(err.statusCode).json({
        code: err.statusCode,
        message: err.message,
        err: err
    });
    return res.status(500).json({ err });
}

module.exports = errorHandlerMiddleware;