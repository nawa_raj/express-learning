require("dotenv").config();
const envData = process.env;

const environment = process.env.NODE_ENV || 'development';
// console.log("running environment: ", environment);

const config = {
    app: {
        PORT: envData.PORT || 8000,
        LOCAL_DB_URI: `mongodb://${envData.LOCAL_DB_USER}:${encodeURIComponent(String(envData.LOCAL_DB_USER_PWD))}@${envData.LOCAL_DB_URL}/${envData.LOCAL_DB_NAME}?${envData.LOCAL_AUTH_SOURCE}`,
        PRODUCTION_DB_URI: envData.MONGO_URI,
        NODE_ENV: environment,
        IS_DEV: String(environment).toLowerCase() === String('development').toLowerCase(),
    }
}

module.exports = config;