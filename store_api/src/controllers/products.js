// imports...
const { asyncWrapper } = require("../utils/asyncWrapper");
const { createCustomError } = require("../helpers/handleError");
const ProductModel = require("../db/models/product");


// start controllers...
const getAllProducts = asyncWrapper(async (req, res, next) => {
    console.log("Search Query: ", req.query);
    const { featured, name, price, company, sort = "name", fields, numericFilters } = req.query;
    const page = Number(req.query.page || 1);
    const perPage = Number(req.query.perPage || 10);
    let totalPages = 0;

    const queryObj = {};

    // validate query values...
    if (featured) queryObj.featured = featured.toLowerCase() === "true" ? true : false;
    if (name) queryObj.name = { $regex: name, $options: 'i' };
    if (company) queryObj.company = { $regex: company, $options: 'i' };
    if (price) queryObj.price = price;

    // handle numeric filters...
    if (numericFilters) {
        const operatorMap = {
            '>': '$gt',
            '>=': '$gte',
            '=': '$eq',
            '<': '$lt',
            '<=': '$lte',
            '!': '$ne',
        }

        const regEX = /\b(<|>|>=|=|<|<=|!)\b/g;
        let filters = String(numericFilters).replace(regEX, (match) => `-${operatorMap[match]}-`);
        const options = ["price", "rating"];
        filters = filters.split(',').forEach((qn) => {
            const [field, operator, value] = qn.split('-');
            if (options.includes(field)) {
                queryObj[field] = { [operator]: Number(value) };
            }
        })
    }

    // sorting and many more...
    let searchResult = ProductModel.find(queryObj);

    // sorting fields...
    if (sort) {
        const sortQueryList = String(sort).trim().split(",").join(" ");
        searchResult = searchResult.find(queryObj).sort(sortQueryList);
    }

    // select fields...
    if (fields) {
        const fieldsList = String(fields).trim().split(",").join(" ");
        searchResult = searchResult.find(queryObj).select(fieldsList);
    }

    // pagination logic...    
    totalPages = await ProductModel.find(queryObj).countDocuments();
    totalPages = Math.ceil(totalPages / perPage);
    const skip = (page - 1) * perPage;
    searchResult = searchResult.skip(skip).limit(perPage);


    // collect results after querying into db...
    products = await searchResult.exec();


    res.status(200).json({ totalPages: totalPages, page: page, perPage: perPage, products });
    // return next(createCustomError("testing now", 400))
});


// exports modules...
module.exports = { getAllProducts };