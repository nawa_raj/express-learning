const asyncWrapper = (fn) => {
    return async (req, res, next) => {
        try {
            return await fn(req, res, next)
        } catch (error) {
            return next(error)
        }
    }
};


const dbConnectionAsyncWrapper = (fn) => {
    return async (errTitle) => {
        try {
            await fn();
        } catch (error) {
            console.log(`${errTitle}: `, error);
        }
    }
}


module.exports = { asyncWrapper, dbConnectionAsyncWrapper };