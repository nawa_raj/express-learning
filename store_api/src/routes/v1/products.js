// imports here...
const { Router } = require("express");
const { getAllProducts } = require("../../controllers/products")

const router = Router();

// routes...
router.route("/").get(getAllProducts);


// export...
module.exports = router;