const express = require("express");
const productRoutes = require("./products");

// app init...
const app = express();


// aggregating routes...
app.use("/products", productRoutes);


module.exports = app;