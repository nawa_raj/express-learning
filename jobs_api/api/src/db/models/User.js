const mongoose = require("mongoose");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const { jwtConfig } = require("../../config");


const userSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "user name is required"],
        minLength: 3,
        maxLength: 50
    },
    email: {
        type: String,
        required: [true, "email is required"],
        match: [
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
            'Please provide a valid email',
        ],
        unique: true,
    },
    password: {
        type: String,
        required: [true, "password is required"],
        minLength: 6,
    },

}, { timestamps: true });


// called pre middleware to hash password...
userSchema.pre("save", async function (next) {
    const salt = await bcrypt.genSalt(10);
    this.password = await bcrypt.hash(this.password, salt);

    // if we remove next() here and from parameter it still work correctly...
    next();
})

// here we implement schema instance method to generate jwt token...
userSchema.methods.createToken = function () {
    const { name, _id } = this;
    const token = jwt.sign({ id: _id, name: name }, jwtConfig.JWT_SECRET, { expiresIn: jwtConfig.JWT_TOKEN_TTL, issuer: jwtConfig.JWT_ISSUER });
    return token;
}

// get user data without password...
userSchema.methods.getUserData = function () {
    const { _id, name, email, createdAt, updatedAt } = this;
    return { _id, name, email, createdAt, updatedAt };
}

// compare password instance method...
userSchema.methods.isPasswordMatch = async function (canditatePassword) {
    const isMatch = await bcrypt.compare(canditatePassword, this.password);
    return isMatch;
}

module.exports = mongoose.model("User", userSchema);