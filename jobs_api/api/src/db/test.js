const mongoose = require('mongoose');
require("dotenv").config({ path: "../../.env" });
const { dbConfig } = require("../config")


// notice the mongoose.createConnection instead of mongoose.connect
const conn = mongoose.createConnection(dbConfig.DB_URI);
conn.on('open', function () {
    conn.db.listCollections().toArray(function (err, collectionNames) {
        if (err) {
            console.log(err);
            return;
        }
        const list = JSON.stringify(collectionNames)
        console.log("Collection List: ", JSON.parse(list));
        conn.close();
    });

});