const CustomAPIError = require("./custom-error");
const { StatusCodes } = require("http-status-codes");


class UnauthenticatedError extends CustomAPIError {
    constructor(message) {
        this.statusCode = StatusCodes.UNAUTHORIZED;
        super(message, this.statusCode);
    }
};

module.exports = UnauthenticatedError;