const CustomAPIError = require("./custom-error");
const { StatusCodes } = require("http-status-codes");


class BadRequestError extends CustomAPIError {
    constructor(message) {
        this.statusCode = StatusCodes.BAD_REQUEST;
        super(message, this.statusCode);
    }
};

module.exports = BadRequestError;