const { asyncWrapper } = require("../utils/asyncWrapper");
const userModel = require("../db/models/User");
const { StatusCodes } = require("http-status-codes");
const { BadRequestError, UnauthenticatedError } = require("../utils/error");


// user register...
const register = asyncWrapper(async (req, res, next) => {
    const user = await userModel.create(req.body);
    res.status(StatusCodes.CREATED).json({ user: user.getUserData(), token: user.createToken() });
});


// user login...
const login = asyncWrapper(async (req, res, next) => {
    const { email, password } = req.body;
    if (!email || !password) return next(BadRequestError("Email & Password is required to Login!"));

    const user = await userModel.findOne({ email });
    if (!user) return next(UnauthenticatedError("Invalid Credentials"));

    const isPasswordCorrect = await user.isPasswordMatch(password);
    if (!isPasswordCorrect) return next(UnauthenticatedError("Invalid Credentials"));
    res.status(StatusCodes.OK).json({ user: user.getUserData(), token: user.createToken() });
});



module.exports = { register, login };