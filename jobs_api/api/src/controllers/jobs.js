const { asyncWrapper } = require("../utils/asyncWrapper");
const { StatusCodes } = require("http-status-codes");
const { NotFoundError, BadRequestError } = require("../utils/error");
const JobModel = require("../db/models/Job");


// get started from here...
const getAllJobs = asyncWrapper(async (req, res, next) => {
    console.log("request method: ", req.method);
    const jobs = await JobModel.find({ createdBy: req.user._id }).sort('createdAt');
    res.status(StatusCodes.OK).json({ count: jobs.length, jobs: jobs });
});


const getJob = asyncWrapper(async (req, res, next) => {
    const { user: { _id: userId }, params: { id: jobId } } = req;
    const job = await JobModel.findOne({ _id: jobId, createdBy: userId })

    if (!job) next(NotFoundError(`No job found with id: ${jobId}`));
    res.status(StatusCodes.OK).json({ job });
});


const createJob = asyncWrapper(async (req, res, next) => {
    req.body.createdBy = req.user._id;
    const job = await JobModel.create(req.body);
    res.status(StatusCodes.CREATED).json({ job });
});


const updateJob = asyncWrapper(async (req, res, next) => {
    const { user: { _id: userId }, params: { id: jobId }, body: { company, position } } = req;

    if (!company || !position) { next(BadRequestError("Company or Position field cannot be empty!")) };
    const job = await JobModel.findByIdAndUpdate({ _id: jobId, createdBy: userId }, req.body, { new: true, runValidators: true });
    if (!job) return next(NotFoundError(`No job found with id: ${jobId}`));
    res.status(StatusCodes.OK).json({ job });
});


const deleteJob = asyncWrapper(async (req, res, next) => {
    const { user: { _id: userId }, params: { id: jobId } } = req;

    const job = await JobModel.findOneAndRemove({ _id: jobId, createdBy: userId });
    if (!job) return next(NotFoundError(`No job found with id: ${jobId}`));
    res.status(StatusCodes.OK).json({ msg: `delete a job successfully with Id: ${jobId}` });
});



module.exports = { getAllJobs, getJob, createJob, updateJob, deleteJob };