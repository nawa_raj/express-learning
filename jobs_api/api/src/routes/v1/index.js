const express = require("express");
const authenticationMiddleware = require("../../middleware/auth");
const authRoutes = require("./auth")
const jobRoutes = require("./jobs")


// init express...
const app = express();


// aggregate routes...
app.use("/auth", authRoutes);
app.use("/jobs", authenticationMiddleware, jobRoutes);


module.exports = app;