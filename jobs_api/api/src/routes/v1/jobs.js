// imports here...
const { Router } = require("express");
const { getAllJobs, getJob, createJob, updateJob, deleteJob } = require("../../controllers/jobs")

const router = Router();


// routes...
router.route("/").get(getAllJobs).post(createJob);
router.route("/:id").get(getJob).patch(updateJob).delete(deleteJob);


// export...
module.exports = router;