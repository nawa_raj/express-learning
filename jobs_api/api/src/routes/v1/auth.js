// imports here...
const { Router } = require("express");
const { register, login } = require("../../controllers/auth")

const router = Router();

// routes...
router.route("/login").post(login);
router.route("/register").post(register);


// export...
module.exports = router;