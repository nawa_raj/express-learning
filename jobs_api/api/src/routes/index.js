const express = require("express");
const v1Routes = require("./v1");
const v2Routes = require("./v2");


// init express...
const app = express();


// aggregate routes...
app.use("/v1", v1Routes);
app.use("/v2", v2Routes);


module.exports = app;