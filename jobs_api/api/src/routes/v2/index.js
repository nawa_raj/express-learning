// imports here...
const { Router } = require("express");
const { asyncWrapper } = require("../../utils/asyncWrapper");

const router = Router();

// routes...
// we use this router as demo version only. we are ging to add Nothing here for now... 
router.route("/welcome").get(asyncWrapper(async (req, res, next) => {
    res.status(200).json({ msg: "Welcome in API version 2" })
}));


// export...
module.exports = router;