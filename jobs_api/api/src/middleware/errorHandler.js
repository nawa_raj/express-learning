// imports 
const { getReasonPhrase, StatusCodes } = require("http-status-codes")


// start...
const errorHandlerMiddleware = (err, req, res, next) => {
    // let define custom error object...
    let customError = { statusCode: err.statusCode || StatusCodes.INTERNAL_SERVER_ERROR, msg: err.message || "Something went wrong try again later" };

    // handling validation error...
    if (err.name === "ValidationError") {
        customError.msg = Object.values(err.errors).map((item) => item.message).join(', ');
        customError.statusCode = 400;
    }

    // handle duplicate error...
    if (err.code && err.code === 11000) {
        customError.msg = `Duplicate value entered for ${Object.keys(err.keyValue)} field, Please Choose another value`;
        customError.statusCode = 400;
    }

    // handle cast error(like object not found in database)...
    if (err.name === "CastError") {
        customError.msg = `No item found with id: ${err.value}`;
        customError.statusCode = 404;

    }

    // return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ err });

    return res.status(customError.statusCode).json({
        status: getReasonPhrase(customError.statusCode),
        status_code: customError.statusCode,
        message: customError.msg,
        error: err
    });
}

module.exports = errorHandlerMiddleware;