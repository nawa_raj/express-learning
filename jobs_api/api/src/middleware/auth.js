const { asyncWrapper } = require("../utils/asyncWrapper");
const { UnauthenticatedError } = require("../utils/error");
const jwt = require("jsonwebtoken");
const { jwtConfig } = require("../config");


const authenticationMiddleware = asyncWrapper(async (req, res, next) => {
    const authHeader = req.headers.authorization;
    if (!authHeader || !String(authHeader).startsWith("Bearer ")) return next(UnauthenticatedError("Authentication Failed!"));

    const token = String(authHeader).split(" ")[1];

    try {
        const payload = jwt.verify(token, jwtConfig.JWT_SECRET, { issuer: jwtConfig.JWT_ISSUER });
        req.user = { _id: payload.id, name: payload.name };
        next();
    } catch (error) {
        next(error)
    }
});


module.exports = authenticationMiddleware;