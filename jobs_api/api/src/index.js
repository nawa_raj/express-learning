const express = require('express');
const notFoundMiddleware = require("./middleware/notFound");
const errorMiddleware = require("./middleware/errorHandler");
const { appConfig, dbConfig } = require("./config/index")
const routes = require("./routes");
const connectDB = require("./db/connect")

// extra security packages...
const helmet = require("helmet");
const cors = require("cors");
const xss = require("xss-clean");
const rateLimiter = require("express-rate-limit");


// app init...
const app = express();


// middlewares...
app.set('trust proxy', 1);
app.use(rateLimiter({
    windowMs: 15 * 60 * 1000, // 15 minutes
    max: 100, // Limit each IP to 100 requests per `window` (here, per 15 minutes)
    standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
    legacyHeaders: false, // Disable the `X-RateLimit-*` headers
}));

app.use(express.static('./public'));
app.use(express.json());

app.use(helmet());
app.use(cors());
app.use(xss());


// routes... 
app.use("/api", routes);


// custome error handler middleware...
app.use(notFoundMiddleware);
app.use(errorMiddleware);


const start = async () => {
    try {
        await connectDB(dbConfig.LOCAL_DB_URI);
        app.listen(appConfig.PORT, console.log(`DB Connected Successfully...\nLocal server is listening on port ${appConfig.PORT}...`));
    } catch (error) {
        console.log("error occured while connect to local db: ", error);
    }
}


start();