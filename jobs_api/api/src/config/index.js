require("dotenv").config();
const envData = process.env;

const environment = process.env.NODE_ENV || 'development';
const local_db_uri = `mongodb://${envData.LOCAL_DB_USER}:${encodeURIComponent(String(envData.LOCAL_DB_USER_PWD))}@${envData.LOCAL_DB_URL}/${envData.LOCAL_DB_NAME}?${envData.LOCAL_AUTH_SOURCE}`;
// console.log("running environment: ", environment);

const config = {
    app: {
        PORT: envData.PORT || 8000,
        NODE_ENV: environment,
        IS_DEV: String(environment).toLowerCase() === String('development').toLowerCase(),
    },
    db: {
        LOCAL_DB_URI: local_db_uri,
        DB_URI: envData.MONGO_URI,
    },
    jwt: {
        JWT_SECRET: envData.JWT_SECRET || "UkXp2s5v8y/B?E(G+KbPeShVmYq3t6w9",
        JWT_ISSUER: envData.JWT_ISSUER || "app_name",
        JWT_TOKEN_TTL: envData.JWT_TOKEN_TTL || "1d",
        JWT_TOKEN_TTL_SECONDS: Number(envData.JWT_TOKEN_TTL_SECONDS || 86400),
    }
}

const appConfig = config.app;
const dbConfig = config.db;
const jwtConfig = config.jwt;

module.exports = { appConfig, dbConfig, jwtConfig };