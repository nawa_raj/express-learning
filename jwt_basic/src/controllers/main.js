const { asyncWrapper } = require("../utils/asyncWrapper");
const { createCustomError } = require("../utils/error/custom-error");
const jwt = require("jsonwebtoken");
const configData = require("../config/index")
const jwtConfig = configData.jwt;


// start from here...
const login = asyncWrapper(async (req, res, next) => {
    const { username, password } = req.body;
    const id = new Date().getDate();

    if (!username || !password) return next(createCustomError(`Please provide username & password`, 400));

    /* we need to specify three parameters: 1.payload to reference the user  2.secretkey  3.options */
    const token = jwt.sign({ id, username }, jwtConfig.JWT_SECRET, { issuer: jwtConfig.JWT_ISSUER, expiresIn: jwtConfig.JWT_TOKEN_TTL })

    res.status(200).json({ msg: "user created", token });
});


const dashboard = asyncWrapper(async (req, res, next) => {
    const { user } = req;
    const luckyNumber = Math.floor(Math.random() * 100);

    res.status(200).json({
        msg: `Hello, ${user.username}`,
        secret: `Here is your authorized data, your lucky number is ${luckyNumber}`
    })
});


module.exports = { login, dashboard };

