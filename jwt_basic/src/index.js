const express = require('express');
const notFoundMiddleware = require("./middleware/notFound");
const errorMiddleware = require("./middleware/errorHandler");
const config = require("./config/index")
const routes = require("./routes");


// app init...
const app = express();


// middlewares...
app.use(express.static('./public'));
app.use(express.json());


// routes... 
app.use("/api", routes);


// custome error handler middleware...
app.use(notFoundMiddleware);
app.use(errorMiddleware);



const start = async () => {
    try {
        // await connectDB(config.app.LOCAL_DB_URI);
        app.listen(config.app.PORT, console.log(`Local server is listening on port ${config.app.PORT}...`));
    } catch (error) {
        console.log("error occured while connect to local db: ", error);
    }
}


start();