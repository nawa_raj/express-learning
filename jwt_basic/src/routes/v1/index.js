const express = require("express");
const { login, dashboard } = require("../../controllers/main");
const authenticationMiddleware = require("../../middleware/auth");

// define router...
const router = express.Router();


// aggregating routes...
router.route("/dashboard").get(authenticationMiddleware, dashboard);
router.route("/login").post(login);


module.exports = router;