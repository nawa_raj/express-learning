// imports...
const { Router } = require("express");
const router = Router();

// route controllers...
const welcomeMessage = require("../controllers/welcome");


// routes...
router.route("/").get(welcomeMessage);


// export...
module.exports = router;