const express = require("express");
const v1Routes = require("./v1");
const welcomeRoutes = require("./welcome");


// init express...
const app = express();


// aggregate routes...
app.use("/v1", v1Routes);
app.use("/", welcomeRoutes);


module.exports = app;