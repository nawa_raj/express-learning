// imports 
const { CustomAPIError } = require("../utils/error");
const { getReasonPhrase, StatusCodes } = require("http-status-codes")


// start
const errorHandlerMiddleware = (err, req, res, next) => {
    // console.log(err);
    if (err instanceof CustomAPIError) return res.status(err.statusCode).json({
        status: getReasonPhrase(err.statusCode),
        status_code: err.statusCode,
        message: err.message,
        err: err
    });
    return res.status(StatusCodes.INTERNAL_SERVER_ERROR).json({ err });
}

module.exports = errorHandlerMiddleware;