const { asyncWrapper } = require("../utils/asyncWrapper");
const { UnauthenticatedError } = require("../utils/error");
const jwt = require("jsonwebtoken");
const config = require("../config/index");


// Start form here...
const jwtConfig = config.jwt;
const authenticationMiddleware = asyncWrapper(async (req, res, next) => {
    // console.log("header: ", req.headers);
    const authHeader = req.headers.authorization;

    if (!authHeader || !authHeader.startsWith("Bearer ")) next(UnauthenticatedError("Auth token not found!"));

    // decode the token data...
    const token = String(authHeader).split(" ")[1];

    try {
        const decoded = jwt.verify(token, jwtConfig.JWT_SECRET);
        const { id, username } = decoded;

        // append user data in request...
        req.user = { id, username };
        next();

    } catch (error) {
        next(UnauthenticatedError("Not Authorized to access this route"));
    }
});

module.exports = authenticationMiddleware;