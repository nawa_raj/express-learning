const CustomAPIError = require("./custom-error");
const { StatusCodes } = require("http-status-codes");


const createCustomError = (msg, statusCode) => { return new CustomAPIError(msg, statusCode) };
const UnauthenticatedError = (msg) => { return new CustomAPIError(msg, StatusCodes.UNAUTHORIZED) };
const BadRequestError = (msg) => { return new CustomAPIError(msg, StatusCodes.BAD_REQUEST) };


module.exports = { CustomAPIError, createCustomError, BadRequestError, UnauthenticatedError };