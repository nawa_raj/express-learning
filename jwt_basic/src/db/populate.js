require("dotenv").config({ path: "../../.env" });
const connectDB = require("./connect");
const config = require("../config")
const ProductModel = require("./models/product");
const jsonProduct = require("../data/products.json");


const start = async () => {
    try {
        await connectDB(config.app.LOCAL_DB_URI);
        await ProductModel.deleteMany();
        await ProductModel.create(jsonProduct);
        console.log(`Successfully Connect to DB...\nIn Total '${jsonProduct.length}' Products added Successfully`);
        process.exit(0);
    } catch (error) {
        console.log("error: ", error);
        process.exit(1);
    }
}


start();