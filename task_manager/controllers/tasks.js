const TaskModel = require("../models/Task");
const { asyncWrapper } = require("../middleware/async");
const { createCustomError } = require("../erros/customError");



// controllers...
const getAllTasks = asyncWrapper(async(req, res, next) => {
    const tasks = await TaskModel.find({});
    res.status(200).json({ tasks });
});


const createTask = asyncWrapper(async(req, res, next) => {
    const task = await TaskModel.create(req.body);
    res.status(201).json({ task });
});


const getTask = asyncWrapper(async(req, res, next) => {
    const { id: taskID } = req.params;
    const task = await TaskModel.findById(taskID).exec();
    if (!task) return next(createCustomError(`No matche item found of id: ${taskID}`, 404));

    res.status(200).json({ task });
});



const deleteTask = asyncWrapper(async(req, res, next) => {
    const { id: taskID } = req.params;
    const task = await TaskModel.findByIdAndDelete(taskID).exec();

    if (!task) return next(createCustomError(`No matche item found of id: ${taskID}`, 404));
    res.status(200).json({ message: "Deleted Successfully" });
});



const updateTask = asyncWrapper(async(req, res, next) => {
    const { id: taskID } = req.params;
    const task = await TaskModel.findByIdAndUpdate({ _id: taskID }, req.body, {
        new: true,
        runValidators: true,
    })
    if (!task) return next(createCustomError(`No record found of id: ${taskID}`, 404));
    res.status(200).json({ message: "succes", task: task });
});



// exports...
module.exports = {
    getAllTasks,
    createTask,
    getTask,
    updateTask,
    deleteTask
}