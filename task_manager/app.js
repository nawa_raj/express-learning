// imports...
const express = require("express");
const { connectRemoteDB, connectLocalDB } = require("./db/connect");
require("dotenv").config();
const tasksRoutes = require("./routes/tasks");
const notFound = require("./middleware/notFound");
const errorHandlerMiddleware = require("./middleware/errorHandler");


// app init...
const app = express();
const port = process.env.PORT || 3000;


// getting .env file data
const mongoDBURI = process.env.MONGO_URI;
// URL FORMAT IS: `mongodb://<db user>:<db user password>@<db server ip>:<port>/<db name>?db auth source`;
const localDBURI = `mongodb://${process.env.LOCAL_DB_USER}:${encodeURIComponent(String(process.env.LOCAL_DB_USER_PWD))}@${process.env.LOCAL_DB_URL}/${process.env.LOCAL_DB_NAME}?${process.env.LOCAL_AUTH_SOURCE}`;


// setup middlewares...
app.use(express.static("./public"));
app.use(express.json());



// routes 
app.use('/api/v1/tasks', tasksRoutes);



// custom error hander middleware...
app.use(notFound);
app.use(errorHandlerMiddleware);



// app start configuration...
const starter = async() => {
    // try {
    //     await connectRemoteDB(mongoDBURI)
    //     app.listen(port, console.log(`Remote server is listening on port ${port}...`));

    // } catch (error) {
    //     console.log("Mongo Atlas Database Connection Error: ", error);
    // }
    try {
        await connectLocalDB(localDBURI);
        app.listen(port, console.log(`Local server is listening on port ${port}...`));
    } catch (error) {
        console.log("Mongo local Database Connection Error: ", error);
    }

}

starter();