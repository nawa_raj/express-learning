// imports 
const { CustomAPIError } = require("../erros/customError");



// start
const errorHandlerMiddleware = (err, req, res, next) => {
    // console.log(err);
    if (err instanceof CustomAPIError) return res.status(err.statusCode).json({ message: err.message });
    return res.status(500).json({ err });
}

module.exports = errorHandlerMiddleware;