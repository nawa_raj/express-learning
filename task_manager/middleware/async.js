const asyncWrapper = (fn) => {
    return async(req, res, next) => {
        try {
            await fn(req, res, next)
        } catch (error) {
            next(error)
        }
    }
};


const dbConnectionAsyncWrapper = (fn) => {
    return async(errTitle) => {
        try {
            await fn();
        } catch (error) {
            console.log(`${errTitle}: `, error);
        }
    }
}


module.exports = { asyncWrapper, dbConnectionAsyncWrapper };