const { Router } = require("express");

// importing controllers...
const { getAllTasks, createTask, getTask, updateTask, deleteTask } = require("../controllers/tasks");

const router = Router();


// routes...
router.route("/").get(getAllTasks).post(createTask);
router.route("/:id").get(getTask).patch(updateTask).delete(deleteTask);


// exporting models...
module.exports = router;

