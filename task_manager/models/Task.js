const { Schema, model } = require("mongoose");

const TaskSchema = new Schema({
    name: {
        type: String,
        required: [true, "name is required"],
        trim: true,
        maxlength: [150, "name cannot be more than 150 characters"]
    },
    completed: {
        type: Boolean,
        required: [true, "completed status is required"],
        // default: false,
    },
})

module.exports = model("Task", TaskSchema);