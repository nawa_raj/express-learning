const mongoose = require("mongoose");



// it connect to the mongo db... 
const connectRemoteDB = (url) => {
    return mongoose.connect(url);
}



// connect local mongo db
const connectLocalDB = (url) => {
    return mongoose.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
}

module.exports = { connectRemoteDB, connectLocalDB };